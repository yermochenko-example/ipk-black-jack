package example;

import java.util.Iterator;

public class Test {
	public interface ProcessElement<T> {
		void process(T element);
	}

	public static <T> void foreach(Iterable<T> iterable, ProcessElement<T> processor) {
		Iterator<T> iterator = iterable.iterator();
		while(iterator.hasNext()) {
			T element = iterator.next();
			processor.process(element);
		}
	}

	public static void main(String[] args) {
		Fibonacci fib = new Fibonacci(15);
		for(Integer k : fib) {
			System.out.println(k);
		}

//		foreach(fib, new ProcessElement<Integer>() {
//			@Override
//			public void process(Integer element) {
//				System.out.println(element);
//			}
//		});

//		foreach(fib, k -> {
//			System.out.println("----");
//			System.out.println(k);
//			System.out.println("----");
//		});

//		foreach(fib, k -> System.out.println(k));

//		foreach(fib, System.out::println);
	}
}
