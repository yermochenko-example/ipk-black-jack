package example;

import java.util.Iterator;

public class Fibonacci implements Iterable<Integer>, Iterator<Integer> {
	private int a = 1;
	private int b = 0;
	private final int n;
	private int k = 0;

	public Fibonacci(int n) {
		System.out.println("DEBUG: Constructor called");
		this.n = n;
	}

	@Override
	public Iterator<Integer> iterator() {
		System.out.println("DEBUG: iterator() called");
		return this;
	}

	@Override
	public boolean hasNext() {
		System.out.println("DEBUG: hasNext() called");
		return k < n;
	}

	@Override
	public Integer next() {
		System.out.println("DEBUG: next() called");
		int c = a + b;
		a = b;
		b = c;
		k++;
		return c;
	}
}
