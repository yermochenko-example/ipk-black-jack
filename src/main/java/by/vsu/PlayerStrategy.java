package by.vsu;

public interface PlayerStrategy {
	int makeBet(int round, int money, Integer[] playersMoney);
	boolean needMore(PlayerHand playerHand);
}
