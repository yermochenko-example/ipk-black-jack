package by.vsu;

import java.util.*;

public class Croupier {
	public static final int CROUPIER_MIN_POINTS = 17;

	private final List<Card> deck = new LinkedList<>();
	private final List<Card> hand = new ArrayList<>();
	private final List<Player> players;

	public Croupier(List<Player> players, int deckCount) {
		this.players = new ArrayList<>(players);
		for(int i = 0; i < deckCount; i++) {
			this.deck.addAll(DeckCardCreator.create());
		}
	}

	public void playGame(int rounds) {
		Collections.shuffle(deck);
		for(int i = 0; i < rounds && !players.isEmpty(); i++) {
			System.out.println("Раунд " + (i + 1) + " начат");
			/* Сбор ставок */
			System.out.println("Делаем ставки");
			Map<Player, Integer> bets = new HashMap<>();
			Integer[] playersMoney = players.stream().map(Player::getMoney).toList().toArray(new Integer[0]);
			Iterator<Player> iterator = players.iterator();
			while(iterator.hasNext()) {
				Player player = iterator.next();
				int bet = player.makeBet(i + 1, playersMoney);
				System.out.print("Игрок " + player.getName() + " сделал ставку " + bet + ". ");
				if(0 < bet && bet <= player.getMoney()) {
					player.setMoney(player.getMoney() - bet);
					bets.put(player, bet);
					System.out.println("Ставка принята");
				} else {
					System.out.println("Ставка не верна. Игрок выбывает из игры.");
					iterator.remove();
				}
			}
			hand.add(deck.remove(0));
			for(Player player : players) {
				/* Игра с одним игроком */
				player.getHand().add(deck.remove(0));
				player.getHand().add(deck.remove(0));
				while(PointsCalculator.calculate(player.getHand()) < PointsCalculator.MAX_POINTS) {
					if(player.needMore()) {
						player.getHand().add(deck.remove(0));
					} else {
						break;
					}
				}
			}
			hand.add(deck.remove(0));
			int croupierPoints;
			while((croupierPoints = PointsCalculator.calculate(hand)) < CROUPIER_MIN_POINTS) {
				hand.add(deck.remove(0));
			}
//			while(PointsCalculator.calculate(hand) < CROUPIER_MIN_POINTS) {
//				hand.add(deck.remove(0));
//			}
//			int croupierPoints = PointsCalculator.calculate(hand);
			/* Подведение итогов */
			iterator = players.iterator();
			while(iterator.hasNext()) {
				Player player = iterator.next();
				int playerPoints = PointsCalculator.calculate(player.getHand());
				if(
						playerPoints == PointsCalculator.MAX_POINTS
						||
						(
								playerPoints < PointsCalculator.MAX_POINTS
								&&
								(
										croupierPoints > PointsCalculator.MAX_POINTS
										||
										croupierPoints <= playerPoints
								)
						)
				) {
					int winning = bets.get(player) * 2;
					System.out.println("Игрок " + player.getName() + " выиграл " + winning);
					player.setMoney(player.getMoney() + winning);
				} else {
					System.out.println("Игрок " + player.getName() + " проиграл");
					if(player.getMoney() == 0) {
						System.out.println("Его деньги закончились. Игрок выбывает из игры.");
						iterator.remove();
					}
				}
			}
			System.out.println("Раунд " + (i + 1) + " окончен");
			System.out.println(players);
			System.out.println("Карты крупье: " + hand);
			for(Player player : players) {
				player.getHand().clear();
			}
			hand.clear();
			System.out.println("-------------------------------------------------------------------------------------------------------");
		}
	}
}
