package by.vsu;

public class Card {
	private final Value value;
	private final Suit suit;

	public Card(Value value, Suit suit) {
		if(value != null && suit != null) {
			this.value = value;
			this.suit = suit;
		} else {
			throw new IllegalArgumentException();
		}
	}

	public Value getValue() {
		return value;
	}

	public Suit getSuit() {
		return suit;
	}

	@Override
	public String toString() {
		return value.getName() + suit.getSymbol();
	}
}
