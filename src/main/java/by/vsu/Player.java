package by.vsu;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Player implements PlayerHand {
	private final String name;
	private final List<Card> hand = new ArrayList<>();
	private int money;
	private final PlayerStrategy strategy;

	public Player(String name, int money, PlayerStrategy strategy) {
		this.name = name;
		this.money = money;
		this.strategy = strategy;
	}

	public String getName() {
		return name;
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int makeBet(int round, Integer[] playersMoney) {
		return strategy.makeBet(round, money, playersMoney);
	}

	public boolean needMore() {
		try {
			return strategy.needMore(this);
		} catch(UnsupportedOperationException e) {
			return false;
		}
	}

	public List<Card> getHand() {
		return hand;
	}

	@Override
	public List<Card> getCards() {
		return List.copyOf(hand);
	}

	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null || getClass() != obj.getClass()) return false;
		Player player = (Player) obj;
		return Objects.equals(name, player.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public String toString() {
		return "Player{" +
				"name='" + name + '\'' +
				", hand=" + hand +
				", money=" + money +
				'}';
	}
}
