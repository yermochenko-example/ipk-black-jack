package by.vsu;

import java.util.ArrayList;
import java.util.List;

public class DeckCardCreator {
	public static List<Card> create() {
		List<Card> deck = new ArrayList<>(Value.values().length * Suit.values().length);
		Value[] values = Value.values();
		Suit[] suites = Suit.values();
		for(Value value : values) {
			for(Suit suit : suites) {
				deck.add(new Card(value, suit));
			}
		}
		return deck;
	}
}
