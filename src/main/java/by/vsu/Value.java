package by.vsu;

public enum Value {
	_2("2", 2),
	_3("3", 3),
	_4("4", 4),
	_5("5", 5),
	_6("6", 6),
	_7("7", 7),
	_8("8", 8),
	_9("9", 9),
	_10("10", 10),
	_J("J", 10),
	_Q("Q", 10),
	_K("K", 10),
	_A("A", 11);

	private final String name;
	private final int points;

	Value(String name, int points) {
		this.name = name;
		this.points = points;
	}

	public int getPoints() {
		return points;
	}

	public String getName() {
		return name;
	}
}
