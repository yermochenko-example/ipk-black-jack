package by.vsu;

import java.util.List;

public interface PlayerHand {
	List<Card> getCards();
}
