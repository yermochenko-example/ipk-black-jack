package by.vsu;

public enum Suit {
	HEARTS('♥'),
	TILES('♦'),
	CLOVERS('♣'),
	PIKES('♠');

	private final char symbol;

	Suit(char symbol) {
		this.symbol = symbol;
	}

	public char getSymbol() {
		return symbol;
	}
}
