package by.vsu;

import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
import java.util.List;

//class PlayerMoneyComparator implements Comparator<Player> {
//	@Override
//	public int compare(Player player1, Player player2) {
//		return Integer.compare(player2.getMoney(), player1.getMoney());
//	}
//}

//class PlayerNameComparator implements Comparator<Player> {
//	@Override
//	public int compare(Player player1, Player player2) {
//		return player1.getName().compareToIgnoreCase(player2.getName());
//	}
//}

public class Runner {
	public static void main(String[] args) {
		List<Player> players = new ArrayList<>(List.of(
			new Player("Вася", 100, new RandomPlayerStrategy()),
			new Player("Аня", 100, new RandomPlayerStrategy()),
			new Player("Петя", 100, new RandomPlayerStrategy())
		));
//		players.sort(new PlayerNameComparator());
//		players.sort(new Comparator<Player>() {
//			@Override
//			public int compare(Player player1, Player player2) {
//				return player1.getName().compareToIgnoreCase(player2.getName());
//			}
//		});
		players.sort((player1, player2) -> player1.getName().compareToIgnoreCase(player2.getName()));
		System.out.println(players);
		Croupier croupier = new Croupier(players, 3);
		croupier.playGame(5);
		//Player player = Collections.max(players);
		//System.out.println(player);
//		players.sort(new PlayerMoneyComparator());
//		players.sort(new Comparator<Player>() {
//			@Override
//			public int compare(Player player1, Player player2) {
//				return Integer.compare(player2.getMoney(), player1.getMoney());
//			}
//		});
		players.sort((player1, player2) -> Integer.compare(player2.getMoney(), player1.getMoney()));
		System.out.println(players);
	}
}
