package by.vsu;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PointsCalculator {
	public static final int MAX_POINTS = 21;
	public static final int ACE_MIN_POINTS = 1;
	public static final int ACE_MAX_POINTS = 11;

	public static int calculate(List<Card> list) {
		/*
		int sum = 0;
		for(Card card : list) {
			sum += card.getValue().getPoints();
		}
		return sum;
		//*/

		/*
		int sum = 0;
		for(Card card : list) {
			if(card.getValue() != Value._A) {
				sum += card.getValue().getPoints();
			} else {
				if(sum + ACE_MAX_POINTS <= MAX_POINTS) {
					sum += ACE_MAX_POINTS;
				} else {
					sum += ACE_MIN_POINTS;
				}
			}
		}
		return sum;
		//*/

		//*
		if(!list.isEmpty()) {
			Set<Integer> aceSums = new HashSet<>();
			aceSums.add(0);
			int sum = 0;
			for(Card card : list) {
				if(card.getValue() != Value._A) {
					sum += card.getValue().getPoints();
				} else {
					Set<Integer> aceMinSums = new HashSet<>();
					Set<Integer> aceMaxSums = new HashSet<>();
					for(Integer aceSum : aceSums) {
						aceMinSums.add(aceSum + ACE_MIN_POINTS);
						aceMaxSums.add(aceSum + ACE_MAX_POINTS);
					}
					aceSums.clear();
					aceSums.addAll(aceMinSums);
					aceSums.addAll(aceMaxSums);
				}
			}
			Set<Integer> sums = new HashSet<>();
			for(Integer aceSum : aceSums) {
				sums.add(aceSum + sum);
			}
			int max = 0;
			int min = Integer.MAX_VALUE;
			for(Integer s : sums) {
				if(s <= MAX_POINTS) {
					if(s > max) {
						max = s;
					}
				} else {
					if(s < min) {
						min = s;
					}
				}
			}
			if(max > 0) {
				return max;
			} else {
				return min;
			}
		} else {
			return 0;
		}
		//*/
	}
}
