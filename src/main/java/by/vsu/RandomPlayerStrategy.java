package by.vsu;

import java.util.Arrays;
import java.util.Random;

public class RandomPlayerStrategy implements PlayerStrategy {
//	private int roundCounts;
//
//	public RandomPlayerStrategy(int roundCounts) {
//		this.roundCounts = roundCounts;
//	}
//
	@Override
	public int makeBet(int round, int money, Integer[] playersMoney) {
		System.out.println("CHECK: round = " + round + /*" / " + roundCounts+ */", " + Arrays.toString(playersMoney));
		Random random = new Random();
//		double x = random.nextDouble();
//		if(x < 0.05) {
//			return -(random.nextInt(money) + 1);
//		} else if(x < 0.1) {
//			return money + random.nextInt(money) + 1;
//		} else {
//			return random.nextInt(money) + 1;
//		}
		return random.nextInt(money) + 1;
	}

	@Override
	public boolean needMore(PlayerHand playerHand) {
		System.out.println("CHECK: " + PointsCalculator.calculate(playerHand.getCards()));
		Random random = new Random();
		return random.nextBoolean();
	}
}
